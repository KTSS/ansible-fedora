# Make bash follow the XDG_CONFIG_HOME specification - Login shell
BASH_LOGIN_CONFIG_DIRECTORY=${XDG_CONFIG_HOME:-$HOME/.config}/bash
BASH_LOGIN_DATA_DIRECTORY=${XDG_DATA_HOME:-$HOME/.local/share}/bash

# Source settings file
if [ -d "$BASH_LOGIN_CONFIG_DIRECTORY" ]; then
    for file in profile bashrc; do
        [ -f "$BASH_LOGIN_CONFIG_DIRECTORY/$file" ] && . "$BASH_LOGIN_CONFIG_DIRECTORY/$file"
    done
fi

# Change the location of the history file by setting the environment variable
[ ! -d "$BASH_LOGIN_DATA_DIRECTORY" ] && mkdir -p "$BASH_LOGIN_DATA_DIRECTORY"
HISTFILE="$BASH_LOGIN_DATA_DIRECTORY/history"

unset BASH_LOGIN_CONFIG_DIRECTORY
unset BASH_LOGIN_DATA_DIRECTORY


# Make bash follow the XDG_CONFIG_HOME specification - Interactive shell
BASH_INTERACTIVE_CONFIG_DIRECTORY=${XDG_CONFIG_HOME:-$HOME/.config}/bash
BASH_INTERACTIVE_DATA_DIRECTORY=${XDG_DATA_HOME:-$HOME/.local/share}/bash

[[ -r "$BASH_INTERACTIVE_CONFIG_DIRECTORY/bashrc" ]] && . "$BASH_INTERACTIVE_CONFIG_DIRECTORY/bashrc"

[[ ! -d "$BASH_INTERACTIVE_DATA_DIRECTORY" ]] && mkdir -p "$BASH_INTERACTIVE_DATA_DIRECTORY"
HISTFILE=$BASH_INTERACTIVE_DATA_DIRECTORY/history

unset BASH_INTERACTIVE_CONFIG_DIRECTORY
unset BASH_INTERACTIVE_DATA_DIRECTORY
